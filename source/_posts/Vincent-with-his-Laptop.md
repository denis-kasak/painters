---
title: Vincent with his Laptop
date: 2023-11-16 22:14:01
tags: "Vincent van Gogh"
---

### Imitated Artist: Vincent van Gogh

{% asset_img artwork.png %}

---
*This image was generated using DreamStudio by Stability AI*
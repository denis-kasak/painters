---
title: Hot Dog
date: 2023-11-18 21:01:11
tags: "Yoshitaka Amano"
---

### Imitated Artist: Yoshitaka Amano

{% asset_img artwork.png %}

---
*Disclaimer: In this particular case the dog is resistant to heat and even enjoys it. Go Dogs!*
*The image(s) was generated using DreamStudio by Stability AI*

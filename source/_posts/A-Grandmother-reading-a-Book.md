---
title: A Grandmother reading a Book
date: 2023-11-18 15:02:12
tags: "Andy Warhol"
---

### Imitated Artist: Andy Warhol

{% asset_img artwork.png %}

---
*The image(s) was generated using DreamStudio by Stability AI*

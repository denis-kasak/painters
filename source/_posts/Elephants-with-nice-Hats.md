---
title: Elephants with nice Hats
date: 2023-11-16 22:30:53
tags: "Vincent van Gogh"
---

### Imitated Artist: Vincent van Gogh

{% asset_img artwork.png %}

---
*This image was generated using DreamStudio by Stability AI*
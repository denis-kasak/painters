---
title: Playful Geometrics
date: 2023-11-18 15:24:15
tags: "Wassily Kandinsky"
---

### Imitated Artist: Wassily Kandinsky

{% asset_img artwork.png %}

---
*The image(s) was generated using DreamStudio by Stability AI*
---
title: Geometric Metropolis
date: 2023-11-18 15:25:29
tags: "Wassily Kandinsky"
---

### Imitated Artist: Wassily Kandinsky

{% asset_img artwork.png %}

---
*The image(s) was generated using DreamStudio by Stability AI*